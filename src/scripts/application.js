"use strict";

require('./register.jsx');

(function() {

  var ready = function() {
    $('nav').pushpin({
      top: $('nav').offset().top,
    });

    $('.scrollspy').scrollSpy({
      scrollOffset: 64,
    });

    $(".button-collapse").sideNav({
      draggable: false,
      closeOnClick: true,
    });
  };

  $(document).ready(ready);
}());

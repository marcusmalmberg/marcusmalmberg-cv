export default [
  {
    visible: true,
    meta: {
      title: 'Lund University',
      subtitle: 'Faculty of Engineering',
      date: '2010 - 2014'
    },
    content: {
      company: 'MSE in Information Technology, specialization in Usability and Design',
      description: `
        <p>
          The main focus of the education was human-computer interaction. With the specialization "Usability and Design" he learnt about usability, interaction design and cognitive science. Here Marcus also chose to broaden his knowledge with additional courses in configuration management and methodologies.
        </p>
        <p>
          As master thesis Marcus chose to focus on Edutainment; how to improve learning with the help of technology and games. The project was interdisciplinary between cognitive science and programming, where the result was a web-based game with adaptive difficulty depending on the player's skills. The game has since been tested with preschool children in southern Sweden with great results.
        </p>
      `,
      tags: [
        'Interaction Design',
        'UX',
        'Usability',
        'Configuration Management',
        'Project Management',
        'Agile Methodologies',
        'Cognitive Science',
        'AI',
        'Math',
      ]
    },
    link: {
      target: 'http://www.lucs.lu.se/wp-content/uploads/2011/12/marcus_malmberg_master_thesis_2014.pdf',
      name: 'Combining Inclusion and Individually Adaptive Learning in an Educational Game for Preschool Children',
    }
  },

  {
    visible: true,
    meta: {
      title: 'Lund University',
      subtitle: 'Faculty of Engineering',
      date: '2007 - 2010'
    },
    content: {
      company: 'Computer Science',
      description: `
        <p>
          In autumn 2007, Marcus started the Master of Science program in Computer Science at LTH where a solid foundation was acquired in areas such as mathematics, programming and algorithms.
        </p>
        <p>
          The programming during the education included everything from assembly language to C and C ++ to Java and frameworks. Mathematics and logic paved the way for algorithms and design patterns - not only know which exists, but also how they work and when to apply them.
        </p>
        <p>
          The key points Marcus brought from the education is to be a problem solver, work in a team and how focus on what is important.
        </p>
      `,
      tags: [
        'Programming',
        'Design Patterns',
        'Algorithms',
        'Math',
        'Java',
        'C / C++',
        'Programming in Teams',
        'Problem Solving',
        'Machine Learning',
        'Network',
        'Computer/Web Security',
      ]
    },
  },

]

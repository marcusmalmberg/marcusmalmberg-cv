export default [
  {
    visible: true,
    meta: {
      title: 'Lead Software Engineer',
      date: '2014 - present'
    },
    content: {
      company: 'Magazinos.com',
      description: `
        <p>
          Magazinos.com is an emerging company that offers its users digital access to thousands of international and local newspapers and magazines. Magazinos cooperates with the largest media houses world wide and its international user base is rapidly growing.
        </p>
        <p>
          Magazinos was initially an idea and needed help to create everything from a prototype, that could be presented to investors, to an international scalable product ready for the end users. The service delivers newspapers and magazines in a digital format to subscribers of the service.
        </p>
        <p>
          Marcus was primarily responsible for setup, development and operation of the project. He has developed everything from the integration of a payment solution, with local currency and recurring payments, to implement a front-end based on sketches from third parties. Marcus work has been business critical for the entire project and it has been important to deliver sustainable, scalable and stable code. A critical part in the beginning of the project was to automate the processing and conversion of PDF files to images, while maintaining a good balance between compression and quality.
        </p>
        <p>
          The result of this project is an international scalable webservice with hundreds of thousands of visitors from every corner of the world. Magazinos is built primarily with Ruby on Rails and the eco system contains multiple microservices running Docker. The highly modular code can be run in parallel which enables it to scale both horizontally as vertically. With well tested code and continuous integration and delivery it makes it easy for new developers to get started in the project.
        </p>
      `,
      tags: [
        'Ruby on Rails',
        'HTML',
        'CoffeeScript',
        'SASS',
        'Docker',
        'Microservices',
        'Payments (Stripe)',
        'Memcache',
        'Redis',
        'Elastic Search',
      ]
    },
    link: {
      target: 'https://www.magazinos.com/',
      name: 'www.magazinos.com',
    }
  },

  {
    visible: true,
    meta: {
      title: 'Fullstack Webdeveloper',
      date: '2014'
    },
    content: {
      company: 'Claire\'s Nordic',
      description: `
        <p>
          Claire's is a jewelry and accessory brand from the United States. Claire's has over 3,500 stores in over 47 countries and their franchise is steadily growing.
        </p>
        <p>
          When the Claire's franchise was about to establish themselves in the Nordic countries they were in need of a website to the local market.
        </p>
        <p>
          In collaboration with an external designer Marcus presented sketches and mockups in line with Claire’s existing Design Guidelines. Marcus’s role was to develop both backend and frontend. He developed a CMS tool that allowed the administrators to edit content directly on the page. This enabled even non-technical persons to easily edit the content, without any previous knowledge of the web and other tools, which was greatly appreciated. Marcus was responsible for maintaining and monitoring the service and took multiple decisions on services and platforms to offer a high accessibility website.
        </p>
        <p>
          The result was a website that is visited by thousands of users and that is constantly growing as they expand and opens new stores.
        </p>
      `,
      tags: [
        'Ruby on Rails',
        'HTML',
        'CoffeeScript',
        'SASS',
        'Heroku',
        'Photoshop',
        'Amazon S3',
      ]
    },
    link: {
      target: 'http://www.clairesnordic.com/',
      name: 'www.clairesnordic.com',
    }
  },

  {
    visible: true,
    meta: {
      title: 'Frontend',
      date: '2016'
    },
    content: {
      company: 'Microcutter.se',
      description: `
        <p>
          Microcutter is a hobby initiative to create a robotic lawn mower based on an open-source mindset. The mower is built with Arduino and Microcutter contributes with both designing and constructing circuits board, as well as software.
        </p>
        <p>
          Lately the custom made Arduino shield has increased in popularity and orders have been placed from multiple different sources, without any order management system. Besides improving the order management there was also a need to showcase the different products in a webshop.
        </p>
        <p>
          Marcus was responsible of creating the entire blog and implement a webshop with an underlying order management system.
        </p>
        <p>
          The shop is built with React and Redux and is served by Ruby on Rails on the server side. This was a pilot project to tryout React in a small scale production environment, where mistakes were allowed to be made. Marcus has been responsible for the entire stack, with everything from hosting to backend, frontend and monitoring.
        </p>
      `,
      tags: [
        'React',
        'Redux',
        'ES2015',
        'SASS',
        'HTML',
      ]
    },
    link: {
      target: 'http://www.microcutter.se/shop',
      name: 'www.microcutter.se/shop',
    }
  },

  {
    visible: false,
    meta: {
      title: 'Architect / Developer',
      date: '2016 - present'
    },
    content: {
      company: 'Payzino',
      description: `
        <p>
          Payzino is a newly started company focused on transactions and money transfers via mobile operators. The company emerged from the need to charge clients that didn’t have access to conventional payment methods, such as credit cards.
        </p>
        <p>
          Payzino needed to create a complete architecture that supports recurring payments and one time checkouts with support for multiple currencies. The platform should be easy to work with, both for our partners as well as our users. Therefore, a lot of focus must be put into creating a solid API.
        </p>
        <p>
          Marcus role was to create a stable, scalable and maintainable system that’s easy to use. He was responsible for everything ranging from architecture and development to delivery. The resulting system is built on microservices running Docker, which are primarily hosted on AWS Elastic Beanstalk, and utilized multiple other services from AWS. Payzino is currently used in production by Magazinos.com.
        </p>
      `,
      tags: [
        'Microservices',
        'Docker',
        'Payments',
        'Cricital Data',
        'AWS EC2',
        'AWS Elastic Beanstalk',
        'AWS VPC',
        'VPN',
      ]
    },
  },

  {
    visible: false,
    meta: {
      title: 'Software- and Solutions Architect',
      date: '2014 - present'
    },
    content: {
      company: 'Magazinos.com',
      description: `
        <p>
          Magazinos är ett ungt företag som erbjuder sina användare tillgång till tusentals digitala magasin och tidningar för läsning online. Magazinos har samarbeten med stora aktörer inom mediabranschen från hela världen och har en växande internationell användarbas.
        </p>
        <p>
          Magazinos var från början en idé och behövde hjälp med att skapa allt från en prototyp som kunde presenteras till investerare till en internationell skalbar produkt redo för slutanvändare. Tjänsten ska leverera tidningar och magasin i ett digitalt format till prenumeranter av tjänsten.
        </p>
        <p>
          Marcus har noga valt ut plattformar, verktyg och tjänster som ska användas i projektet för att kunna skapa en skalbar, hållbar och stabil tjänst. Till att börja med satte han upp
        </p>
      `,
      tags: [
        'Microservices',
        'Rollbar',
        'New Relic',
        'UML',
        'ERD',
        'Docker',
        'Amazon Web Services',
        'Heroku',
        'Google Analytics',
        'Google Apps',
        'Configuration Management',
      ]
    },
    link: {
      target: 'https://www.magazinos.com',
      name: 'www.magazinos.com',
    }
  },

  {
    visible: true,
    meta: {
      title: 'Co-founder / IT Entrepreneur',
      date: '2011 - present'
    },
    content: {
      company: 'Codeleaf',
      description: `
        <p>
          Codeleaf is a consultancy company that was founded by Marcus and two colleagues from the management group of the student consulting company, Lunicore. Codeleaf’s business idea started as a way to finance their own products by offering their services as consultants. In recent years their concept has evolved into investing in interesting companies and startups with expertise and manpower. This has enabled them to be a partner in various startups in the Nordic countries.
        </p>
        <p>
          One of Codeleaf’s strengths are that they are interested in, and are updated at, new technologies and information. Marcus, with his master in user experience and interaction design, was quick to adopt the responsive design and apply it to their projects. Codeleaf was thus one of the first companies to offer this to kind of experience to customers in the region of Öresund.
        </p>
        <p>
          Codeleaf has been thriving with Marcus in the lead and have delivered multiple exciting, demanding and technically challenging projects. Because of Marcus experience and knowledge Codeleaf has been able to take on several projects and customers, that otherwise would not have been possible.
        </p>
      `,
      tags: [
        'Windows',
        'Mac OS X',
        'MS Office',
        'Linux',
        'Adobe',
        'Amazon Web Services',
        'Google Analytics',
        'New Relic',
        'Rollbar',
        'Configuration Management',
        'Business Development',
        'Google Apps',
      ]
    },
    link: {
      target: 'http://www.codeleaf.se',
      name: 'www.codeleaf.se',
    }
  },

  {
    visible: true,
    meta: {
      title: 'Software Developer',
      date: '2011'
    },
    content: {
      company: 'Nibe Energy Systems',
      description: `
        <p>
          NIBE Energy Systems is the largest manufacturer of domestic heating products in the Nordic countries and a market leader in Northern Europe in the electric water heater and heat pump segments. Their mission is to supply homes and buildings with products that provide domestic hot water and ensure a comfortable indoor climate.
        </p>
        <p>
          Their innovation department collects huge amounts of data and was in need of better and more powerful tools to increase their day-to-day efficiency at the office.
        </p>
        <p>
          Marcus identified their workflow and could in collaboration with the client propose a solution that would solve their problem. Marcus developed a tool in Delphi that could read large amount of data from their Excel files. The tool in turn analyzed, processed and presented the data to the user in a natural and friendly way as well as enabled the user easily work with it directly in the tool.
        </p>
        <p>
          The primary goal of the project was to create reports according to the European standards from the provided data. The tool was specifically tailored to automatically process and analyze their huge amount of data to produce a standardized report in PDF format. The report used statistical methods and presented the data in a natural and clear way that a non-technical person could interpret the result.
        </p>
        <p>
          The tool that Marcus created is used in their daily work and can save several hours of work each day per user. The European organization that reviews the reports have given great commendation for its layout and form. The client was very pleased with the tool and collaboration and has since returned several times to further develop it and adopt it to additional departments, situations and standards.
        </p>
      `,
      tags: [
        'Delphi',
        'Excel VBA',
        'Excel OLE',
        'Embarcadero RAD Studio',
      ]
    },
  },

  {
    visible: false,
    meta: {
      title: 'Project Manager / Android Developer',
      date: '2016 Q3'
    },
    content: {
      company: 'Magazinos.com - Android App',
      description: `
        <p>
          Magazinos är ett ungt företag som erbjuder sina användare tillgång till tusentals digitala magasin och tidningar för läsning online. Magazinos har samarbeten med stora aktörer inom mediabranschen från hela världen och har en växande internationell användarbas.
        </p>
        <p>
          I takt med att Magazinos växte och fick fler användare ökade behovet av att komplettera webbtjänsten med en Android app.
        </p>
        <p>
          Marcus roll var att projektleda ett juniort team av android-utvecklare. Mot slutet av projektet hjälpte Marcus även till som en extra resurs för Android-utveckling, buggfixning, underhåll samt publicering på Google Play.
        </p>
      `,
      tags: [
        'Android',
        'Gradle',
        'Google Services',
        'RESTful API',
        'Swagger.io',
      ]
    },
    link: {
      target: 'https://play.google.com/store/apps/details?id=com.zuccero.magazinos',
      name: 'Magazinos (Google Play)',
    }
  },

  {
    visible: true,
    meta: {
      title: 'Quality Assurance Manager IT / Project Leader',
      date: '2011'
    },
    content: {
      company: 'Lunicore',
      description: `
        <p>
          Lunicore is one of the Scandinavia’s leading and largest student consultancy company with Lund’s University as its largest shareholder.
        </p>
        <p>
          As the company grew and more IT related projects were requested by clients, Lunicore was in need of a new combined role of Quality Assurance Manager and project lead to handle the rapidly increasing business.
        </p>
        <p>
          When Marcus assumed the role of QAM, there were no established processes and workflows. One of the first measures that Marcus took was that all projects would be version managed with Git and organized training and education for all consultants. There was no clear process on how an IT project should be executed and Marcus therefore created a general flow. The flow made it easier for both new consultants to get into the role as a consultant as it simplified for existing consultants to collaborate in their projects.
        </p>
        <p>
          The combined role included everything from identifying the customer’s needs and create a first draft of the project proposal, to compose a complete team of developers and roles based on their current occupancy and skills. Marcus was the customer's main contact and made sure that the projects proceeded as planned.
        </p>
        <p>
          Marcus’ work with processes and versioning gave good results and allowed the IT department to significantly grow. This resulted in financial gain for the company as the IT department was able to double its sales and deliver more successful projects on time. The situation of the consultants was considerably improved by the existence of processes to follow. Using common tools increased the cooperation between the consultants and reduced the threshold to familiarize themselves with new and existing projects, which was highly appreciated by the consultants.
        </p>
      `,
      tags: [
        'Configuration Management',
        'Git',
        'Project Lead',
        'Planning',
        'Agile',
        'Customer Relations',
      ]
    },
  },

  {
    visible: false,
    meta: {
      title: 'Frontend / UX',
      date: '2012'
    },
    content: {
      company: 'MeWe / AdWall',
      description: `
        <p>
          Den danska reklambyrån MeWe Group efterfrågade, i samarbete med NCC och Clear Channel, hjälp att ta fram ett nytt koncept. Konceptet AdWall skulle bli en digital interaktiv vägg som tillät privatpersoner och företag att sätta upp reklam på väggar genom att ladda upp innehåll via ett webbgränssnitt.
        </p>
        <p>
          Marcus roll i projektet var i första steget att ta fram UX och design till tjänsten för att i ett senare skede implementera frontend. Marcus skapade en kollageliknande interaktiv vägg där man som besökare kunde ladda upp och beskära bilder som sedan presenterades på väggen. Han implementera en betallösning i ePay som tillät användarna att köpa plats på väggen. Marcus var även huvudansvarig för matematik och utveckling i den metod som hanterade att omvandla den digitala väggen till en fil som kunde skickas till tryckeri.
        </p>
        <p>
          AdWall blev en webbaserad interaktiv digital vägg där besökare kunde köpa reklamplats och ladda upp sina bilder och budskap till väggen. Den digitala väggen processerades och genererade filer som automatiskt skickades till tryckeri för att sedan klistras upp på utvalda väggar.
        </p>
      `,
      tags: [
        'HTML',
        'Coffeescript',
        'SASS',
        'Bootstrap',
        'Photoshop',
        'JQuery',
        'Ruby on Rails',
        'ImageMagick',
        'Payments (ePay)',
      ]
    },
  },

  {
    visible: true,
    meta: {
      title: 'Fullstack Webdeveloper / System Architect',
      date: '2013 - 2014'
    },
    content: {
      company: 'Coach-o-Matic',
      description: `
        <p>
          Caochomatic offers professional coaching by telephone to individuals and businesses.
        </p>
        <p>
          The company needed help creating a web-based interface that coordinates booking and payments as well as providing its customers with tools for self-coaching online. When the company got in touch with Marcus it was only an idea and he also contributed with business and concept development.
        </p>
        <p>
          Marcus's role was to design the system, both in terms of choosing technologies, environments and platforms as modeling databases and resources. He worked closely with a graphic designer to develop sketches, user flows and graphic design of the service. Based on that material he contributed to everything from server configuration to development of the backend and frontend as to delivery and testing. Marcus, who had a leading role in the team delegated duties to part-time developers and other partners in the project. He coordinated the ongoing changes according to the client's and the team's needs and requirements.
        </p>
        <p>
          The project was a joint venture with Codeleaf and the company can today offer its users a simple form of self-coaching online, as well as a specially designed booking system for coaches and clients.
        </p>
      `,
      tags: [
        'Ruby on Rails',
        'ERD',
        'Javascript',
        'HTML',
        'CSS',
        'D3.js',
        'JQuery',
      ]
    },
  },

  {
    visible: false,
    meta: {
      title: 'Software Developer',
      date: 'Q3 2010 - Q4 2010'
    },
    content: {
      company: 'Kullander',
      description: `
        <p>
          Kullander är ett konsultföretag och södra Sveriges största Apple-partner.
        </p>
        <p>
          Kullander utvecklade ett nytt internt butikssystem för att hantera kunder, ordrar och lager. De fick in ett större uppdrag och hade inte tillräckligt med egna konsulter tillgängliga och behövde således ta in extern hjälp med att färdigställa deras nya system.
        </p>
        <p>
          Marcus var en del i ett större team där han utvecklade moduler till butikssystemet. Stora delar av projektet var redan implementerat och processer, arkitektur och flöde var redan definierat. Marcus roll i projektet var framförallt utveckling av kundhanteringsmodulen.
        </p>
        <p>
          Flera moduler till affärssystemet kunde levereras enligt plan. Innan detta projekt hade Marcus ingen tidigare erfarenhet av utveckling i XCode eller Objective-C, men lärde sig snabbt grunderna och kunde leverera över förväntningar.
        </p>
      `,
      tags: [
        'Objective-C',
        'Xcode',
      ]
    },
  },
]

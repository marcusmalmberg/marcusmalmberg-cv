export default {
  frontend: {
    name: 'FRONTEND',
    percentage: '84',
    ratings: [
      {
        name: 'JavaScript',
        filledStars: 4,
      },
      {
        name: 'CoffeeScript',
        filledStars: 4,
      },
      {
        name: 'CSS / SASS',
        filledStars: 4,
      },
      {
        name: 'ES2016',
        filledStars: 3,
      },
      {
        name: 'React',
        filledStars: 3,
      },
      {
        name: 'TypeScript',
        filledStars: 2,
      },
    ],
  },

  backend: {
    name: 'BACKEND',
    percentage: '92',
    ratings: [
      {
        name: 'Ruby on Rails',
        filledStars: 5,
      },
      {
        name: 'PHP',
        filledStars: 3,
      },
      {
        name: 'Java',
        filledStars: 3,
      },
      {
        name: 'Delphi',
        filledStars: 3,
      },
      {
        name: 'Android',
        filledStars: 3,
      },
      {
        name: 'C++',
        filledStars: 2,
      },
    ],
  },

  misc: {
    name: 'OTHER',
    percentage: '79',
    ratings: [
      {
        name: 'Heroku',
        filledStars: 5,
      },
      {
        name: 'AWS',
        filledStars: 4,
      },
      {
        name: 'Webpack',
        filledStars: 4,
      },
      {
        name: 'Microservices',
        filledStars: 3,
      },
      {
        name: 'Architecture',
        filledStars: 3,
      },
      {
        name: 'Docker',
        filledStars: 2,
      },
    ],
  },

  completeLists: {
    leftCol: [
      {
        name: 'BUSINESS AREAS',
        condensed: false,
        entries: [
          'Web development',
          'Software architecture',
          'Solutions architecture',
          'System development',
          'System integration',
          'Database design',
          'System design',
          'Project management',
          'Team leader',
        ]
      },
      {
        name: 'INDUSTRIES',
        condensed: false,
        entries: [
          'Media',
          'Hotel & Restaurant',
          'Retail',
          'Security & Surveillance',
          'Innovation',
          'HVAC',
          'Heavy Industry',
        ]
      },
      {
        name: 'LANGUAGES AND FRAMEWORKS',
        condensed: true,
        entries: [
          'Java',
          'Ruby',
          'Delphi',
          'Excel VBA',
          'Objective-C',
          'C++',
          'C',
          'C#',
          'Python',
          'Perl',
          'jMonkey',
          'Bash',
          'Flash',
          'Matlab',
        ]
      },
      {
        name: 'WEB AND MOBILE DEVELOPMENT',
        condensed: true,
        entries: [
          'Ruby on Rails',
          'PHP',
          'CSS',
          'SCSS',
          'HTML4',
          'HTML5',
          'Javascript',
          'Coffeescript',
          'Android',
          'iOS',
          'PhoneGap',
          'Appcelerator',
          'Electron',
          'JQuery',
          'AJAX',
          'Twitter Bootstrap',
          'Material Design',
          'Open Graph',
          'PhaserIO',
          'NodeJS',
          'D3.js',
          'React',
          'Redux',
          'Angular',
          'Angular2',
          'ES2015',
          'ES2106',
          'TypeScript',
        ]
      },
      {
        name: 'ARCHITECTURE & DESIGN',
        condensed: true,
        entries: [
          'UML',
          'ERD',
          'PERT',
          'Gantt',
        ]
      },
      {
        name: 'DATABASES',
        condensed: true,
        entries: [
          'SQL programming',
          'MySQL',
          'PostgreSQL',
          'SQLite',
        ]
      },
    ],
    rightCol: [
      {
        name: 'METHODOLOGIES',
        condensed: true,
        entries: [
          'LEAN',
          'Agile',
          'Extreme programming',
          'Continuous integration',
          'Continuous delivery',
          'SCRUM',
          'Pair programming',
          'TDD/BDD',
        ]
      },
      {
        name: 'CONFIGURATION MANAGEMENT',
        condensed: true,
        entries: [
          'Git',
          'Subversion',
          'CVS',
          'GitHub',
          'BitBucket',
        ]
      },
      {
        name: 'TESTING',
        condensed: true,
        entries: [
          'Codeship',
          'CircleCI',
          'SimpleCov',
          'Coveralls',
          'RSpec',
          'Cucumber',
          'Jasmin',
          'Capybara',
          'Poltergeist',
        ]
      },
      {
        name: 'PRODUCTS/ENVIRONMENTS',
        condensed: true,
        entries: [
          'Microsoft',
          'Windows',
          'MS Office',
          'Linux',
          'UNIX',
          'Ubuntu',
          'OSX',
          'Bash on Windows',
          'WSL',
          'tmux',
          'Adobe Photoshop',
          'Adobe Acrobat Pro',
          'Adobe Illustrator',
          'Apache',
          'nginx',
          'Eclipse',
          'SSH',
          'SSL',
          'VPN',
          'OpenSwan',
          'Aptana',
          'Atom',
          'Skype',
          'Slack',
          'Screenhero',
          'PivotalTracker',
          'Trello',
          'Zendesk',
          'Google Apps',
          'AWS (IAM, S3, EC2, VPC, VPN, SQS, SNS, Elastic Beanstalk)',
          'Heroku',
          'Firebase Hosting',
          'Elastic Search',
          'Stripe Payments',
          'ePay',
          'XCode',
          'Google Analytics',
          'SendGrid',
          'MailChimp',
          'Twilio',
          'VirtualBox',
          'VMWare',
          'WireShark',
          'Balsamiq Mockups',
          'InvisionApp',
          'Redis',
          'Memcache',
          'Rollbar',
          'NewRelic',
          'Papertrail',
          'Embarcadero RAD Studio',
        ]
      },
      {
        name: 'MISC',
        condensed: true,
        entries: [
          'Graphic design',
          'Image editing',
          'Shell scripts',
          'Regexp',
          'DNS',
          'UX',
          'UI',
          'REST WS',
          'JSON',
          'API',
          'Algorithms',
          'Design Patterns',
          'Ghostscript',
          'ImageMagick',
          'Machine Learning',
          'Web Security',
          'Data Security',
        ]
      },
      {
        name: 'SPECIAL EXPERTISE',
        condensed: true,
        entries: [
          'Extensive knowledge of web development in Ruby on Rails',
          'UX-design and system integration',
          'Software and solutions architecture',
        ]
      },
    ],
  }
}

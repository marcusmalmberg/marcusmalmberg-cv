import React from 'react';
import ReactDOM from 'react-dom';

import TimelineComponent from './timeline/timeline.component';
import AbilitiesComponent from './abilities/abilities.component';

import sampleOfQualifications from './database/sample-of-qualifications.database.js'
import education from './database/education.database.js'
import abilities from './database/abilities.database.js'


ReactDOM.render(
  <div>
    <TimelineComponent elementId="sample-of-qualifications" heading="Sample of Qualifications" entries={ sampleOfQualifications } />
    <AbilitiesComponent elementId="skills-abilities" heading="Skills & Abilities" abilities={ abilities } />
    <TimelineComponent elementId="education" heading="Education" entries={ education } />
  </div>,
  document.getElementById('app-root')
);

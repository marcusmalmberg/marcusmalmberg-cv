import React, { PropTypes } from 'react';

export default class TimelineItemComponent extends React.Component {
  static propTypes = {
    entry: PropTypes.object.isRequired,
    isLast: PropTypes.bool.isRequired,
  };

  link_tag() {
    const { link } = this.props.entry

    if (link && link.target && link.name) {
      return (
        <p>
          <a href={ link.target }>{ link.name }</a>
        </p>
      )
    } else {
      return null;
    }
  }

  render() {
    const { entry, isLast } = this.props

    if (!entry.visible) {
      return null;
    }

    return (
      <div className="row no-margin-bottom">
        <div className="col m3 hide-on-small-only entry-meta">
          <h6 className="title">{ entry.meta.title }</h6>
          { entry.meta.subtitle && <div className="sub-title">{ entry.meta.subtitle }</div> }
          <div className="date">{ entry.meta.date }</div>
        </div>
        <div className="col m9 s11 push-s1 entry-summary">
          <h6 className="title">{ entry.content.company }</h6>

          <div dangerouslySetInnerHTML={{ __html: entry.content.description }} ></div>

          { this.link_tag() }

          <div className="tag-cloud">
            { entry.content.tags.map((tag, i) => {
              return <div className="chip" key={i}>{ tag }</div>
            })}
          </div>

          { !isLast && <div className="divider"></div> }

        </div>
      </div>
    )
  }
}

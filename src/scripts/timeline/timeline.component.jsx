import React, { PropTypes } from 'react';

import TimelineItemComponent from './timeline.item.component';

export default class TimelineListComponent extends React.Component {
  static propTypes = {
    entries: PropTypes.array.isRequired,
    heading: PropTypes.string.isRequired,
    elementId: PropTypes.string.isRequired,
  };

  constructor() {
    super();

    this.state = {
      visibleEntries: [],
    }
  }

  componentDidMount() {
    this.filterVisibleEntries();
  }

  filterVisibleEntries() {
    const visibleEntries = this.props.entries.filter((entry) => {
      return entry.visible
    });

    this.setState({
      visibleEntries,
    });
  }

  isLast(i) {
    return i === this.state.visibleEntries.length - 1;
  }

  render() {
    const { entries, heading, elementId } = this.props;

    return (
      <div className="section timeline scrollspy" id={ elementId }>
        <div className="container">
          <div className="card">
            <div className="card-content">
              <h4 className="center">{ heading }</h4>
              {this.state.visibleEntries.map((entry, i) => {
                return <TimelineItemComponent key={ i } entry={ entry } isLast={ this.isLast(i) } />
              })}
            </div>
          </div>
        </div>
      </div>
    )
  }
}

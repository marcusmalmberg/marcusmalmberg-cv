import React, { PropTypes } from 'react';

export default class RatingTableComponent extends React.Component {
  static propTypes = {
    ratings: PropTypes.array.isRequired,
  };

  render() {
    const { ratings } = this.props

    return (
      <table className="condensed">
        <tbody>
          {ratings.map((rating, i) => {
            return (
              <tr key={ rating.name }>
                <td className="tech">
                  { rating.name }
                </td>
                <td>
                  {[0,1,2,3,4].map((i) => {
                    return <i key={i} className={`material-icons ${ rating.filledStars > i ? 'filled' : '' }`}>star</i>
                  })}
                </td>
              </tr>
            )
          })}
        </tbody>
      </table>
    )
  }
}

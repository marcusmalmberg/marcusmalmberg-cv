import React, { PropTypes } from 'react';

export default class CircleComponent extends React.Component {
  static propTypes = {
    percentage: PropTypes.string.isRequired,
  };

  render() {
    const { percentage } = this.props

    return (
      <div className={`center c100 p${ percentage }`}>
        <span>{ percentage }%</span>
        <div className="slice">
          <div className="bar"></div>
          <div className="fill"></div>
        </div>
      </div>
    )
  }
}

import React, { PropTypes } from 'react';

import CircleComponent from './circle.component';
import RatingTableComponent from './rating-table.component';

export default class AbilitiesComponent extends React.Component {
  static propTypes = {
    abilities: PropTypes.object.isRequired,
    heading: PropTypes.string.isRequired,
    elementId: PropTypes.string.isRequired,
  };

  render() {
    const { abilities, heading, elementId } = this.props

    const highlighted = [abilities.frontend, abilities.backend, abilities.misc]
    const completeLists = abilities.completeLists

    return (
      <div className="section scrollspy" id={ elementId }>
        <div className="container">
          <div className="card">
            <div className="card-content">
              <h4 className="center">{ heading }</h4>

              <div className="row">
                {highlighted.map((field, i) => {
                  return (
                    <div className="col m4 s12" key={ field.name }>
                      <h5 className="center">{ field.name }</h5>

                      <CircleComponent percentage={ field.percentage } />
                      <RatingTableComponent ratings={ field.ratings } />

                    </div>
                  )
                })}
              </div>

              <div className="divider"></div>

              <div className="row">
                <div className="col l5 push-l1 m6 s12">
                  { this.renderColumn(completeLists.leftCol) }
                </div>

                <div className="col l5 push-l1 m6 s12">
                  { this.renderColumn(completeLists.rightCol) }
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
    )
  }

  renderColumn(fields) {
    return fields.map((field, i) => {
      let list;
      if (field.condensed) {
        list = (
          <li>
            { field.entries.join(', ') }
          </li>
        )
      } else {
        list = (field.entries.map((entry, i) => {
          return (
            <li key={ entry }>{entry}</li>
          )
        }))
      }

      return (
        <div key={ field.name }>
          <span className="strong">{ field.name }</span>
          <ul>
            { list }
          </ul>
        </div>
      )
    })
  }

}

// Hack for Ubuntu on Windows: interface enumeration fails with EINVAL, so return empty.
// https://github.com/Microsoft/BashOnWindows/issues/468#issuecomment-247684647
try {
  require('os').networkInterfaces()
} catch (e) {
  require('os').networkInterfaces = () => ({})
}

const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const path = require('path');

const devBuild = process.env.NODE_ENV !== 'production';
const nodeEnv = devBuild ? 'development' : 'production';

const watchOptions = {}
if (process.env.POLL_WATCH === 'true') {
  watchOptions.poll = 500;
  watchOptions.aggregateTimeout = 500;
}

const config = {
  entry: [
    'babel-polyfill',
    './src/scripts/application.js',
    './src/styles/application.scss',
  ],

  output: {
    filename: 'application.js',
    path: 'public/assets',
  },

  resolve: {
    extensions: ['', '.js', '.jsx'],
    alias: {
      react: path.resolve('./node_modules/react'),
      'react-dom': path.resolve('./node_modules/react-dom'),
    },
  },

  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify(nodeEnv),
      },
    }),
    new ExtractTextPlugin('application.css', {
      allChunks: true
    }),
  ],

  module: {
    loaders: [
      {
        test: /\.scss$/,
        loader: ExtractTextPlugin.extract('css!sass?includePaths[]=' + path.resolve(__dirname, "./node_modules/compass-mixins/lib")),
      },
      {
        test: /\.jsx?$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
      }
    ],
  },

  watchOptions: watchOptions,
}

module.exports = config;

if (devBuild) {
  module.exports.devtool = 'eval-source-map';
}
